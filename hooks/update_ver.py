#!/usr/bin/env python

import os
import sys
from xml.etree import ElementTree
from pkg_resources import parse_version

ElementTree.register_namespace('', 'http://www.w3.org/ns/widgets')
tree = ElementTree.parse('./config.xml')
root = tree.getroot()
version = root.attrib['version']

def versiontuple(v):
    return tuple(map(int, (v.split("."))))
parsed_semver = versiontuple(version)
build_number = parsed_semver[2]
build_number += 1

semver = '%s.%s.%s' % (parsed_semver[0], parsed_semver[1], build_number)

print(semver)
root.attrib['version'] = semver


tree.write('./config.xml')

print "Updated build number to %s!" % (build_number)
